<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Contracts
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Contracts;


interface BeOrdersProductAdapter
{
    /** id
     * @return string
     * */
    public function getId();

    /** name
     * @return string
     * */
    public function getName();

    /** price
     * @return string
     * */
    public function getPrice();

    /** quantity
     * @return string
     * */
    public function getQuantity();

    /** total_price
     * @return string
     * */
    public function getTotalPrice();

    /** url
     * @return string
     * */
    public function getLink();

    /** measure_unit (шт. упаковка.)
     * @return string
     * */
    public function getMeasureUnit();

    /** article
     * @return string|null
     * */
    public function getArticle();

}