<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Contracts
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Contracts;


interface BeAdapter
{
    /** system type
     * @return string
     * */
    public function getSystem();
}