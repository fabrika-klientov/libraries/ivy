<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Contracts
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Contracts;


interface BeOrdersAdapter extends BeAdapter
{
    /** id order
     * @return int
     * */
    public function getOrderId();

    /** date_created order
     * @return string
     * */
    public function getDateCreated();

    /** client_first_name client
     * @return string
     * */
    public function getFirstName();

    /** client_second_name client
     * @return string
     * */
    public function getSecondName();

    /** client_last_name client
     * @return string
     * */
    public function getLastName();

    /** full_name client
     * @param bool $withSecondName
     * @return string
     * */
    public function getFullName(bool $withSecondName = true);

    /** email client
     * @return string
     * */
    public function getEmail();

    /** phone client
     * @param bool $normalize
     * @return string
     * */
    public function getPhone(bool $normalize = true);

    /** delivery
     * @return string
     * */
    public function getDeliveryId();

    /** delivery
     * @return string
     * */
    public function getDeliveryType();

    /** delivery
     * @return string
     * */
    public function getDeliveryAddress();

    /** payment
     * @return string
     * */
    public function getPaymentId();

    /** payment
     * @return string
     * */
    public function getPaymentType();

    /** price
     * @return string
     * */
    public function getPrice();

    /** comment notes
     * @return string
     * */
    public function getComment();

    /** products
     * @return \Illuminate\Support\Collection
     * */
    public function getProducts();

    /** status
     * @return string
     * */
    public function getStatus();

    /** source
     * @return string
     * */
    public function getSource();
}