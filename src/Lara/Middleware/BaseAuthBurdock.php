<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lara
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Lara\Middleware;

use Closure;
use Ivy\IvyService;
use Lantana\Extensions\Guard\GuardService;

abstract class BaseAuthBurdock
{
    /**
     * @var IvyService $ivyService
     */
    protected $ivyService;

    /**
     * @var GuardService $guardService
     */
    protected $guardService;

    /**
     * Create a new middleware instance.
     *
     * @param  IvyService $ivyService
     * @param GuardService $guardService
     * @return void
     */
    public function __construct(IvyService $ivyService, GuardService $guardService)
    {
        $this->ivyService = $ivyService;
        $this->guardService = $guardService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $this->init($request);

        return $next($request);
    }

    /**
     * @param \Illuminate\Http\Request  $request
     * @return bool
     * */
    abstract protected function init($request);
}