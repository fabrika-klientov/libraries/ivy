<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lara
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Lara\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Ivy\IvyService;

class ServiceProvider extends BaseServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IvyService::class, function ($app) {
            return new IvyService();
        });
    }
}
