<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Ivy
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy;


use Ivy\Helpers\InstancesAdapter;
use Ivy\Helpers\ActionsAdapter;

class IvyService
{
    use ActionsAdapter, InstancesAdapter;

    /**
     * @var static $service
     * */
    protected static $service;

    /**
     * @return void
     * */
    public function __construct()
    {
        self::$service = $this;
    }


    /**
     * @return static|null
     * */
    public static function context()
    {
        return self::$service ?? null;
    }

}