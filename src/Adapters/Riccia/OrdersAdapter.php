<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Adapters\Riccia;


use Riccia\Adapters\OrdersAdapter as RicciaOrdersAdapter;
use Ivy\Adapters\Helpers\Normalize;
use Ivy\Contracts\BeOrdersAdapter;
use Ivy\Contracts\BePrimary;
use Riccia\Client;
use Riccia\Models\Orders;

class OrdersAdapter extends RicciaOrdersAdapter implements BePrimary, BeOrdersAdapter
{
    use Normalize;

    /**
     * @var array $configs
     * */
    protected $configs;

    /**
     * @param Orders $data
     * @param Client $client
     * @param array $configs
     * @return void
     * */
    public function __construct(Orders $data, Client $client = null, array $configs = null)
    {
        parent::__construct($data, $client);
        $this->configs = $configs ?? (isset($client) ? $client->getHttpClient()->getAuth() : []);
    }

    /**
     * @return string
     */
    public function getSystem()
    {
        return 'riccia';
    }

    /**
     * @param mixed $data
     * @return bool
     * */
    public static function primary($data): bool
    {
        return $data instanceof Orders;
    }
}