<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Adapters\Crocus;


use Crocus\Client;
use Crocus\Models\Orders;
use Crocus\Adapters\OrdersAdapter as CrocusOrdersAdapter;
use Ivy\Adapters\Helpers\Normalize;
use Ivy\Contracts\BeOrdersAdapter;
use Ivy\Contracts\BePrimary;

class OrdersAdapter extends CrocusOrdersAdapter implements BePrimary, BeOrdersAdapter
{
    use Normalize;

    /**
     * @var array $configs
     * */
    protected $configs;

    /**
     * @param Orders $data
     * @param Client $client
     * @param array $configs
     * @return void
     * */
    public function __construct(Orders $data, Client $client = null, array $configs = null)
    {
        parent::__construct($data, $client);
        $this->configs = $configs ?? (isset($client) ? $client->getHttpClient()->getAuth() : []);
    }

    /**
     * @return string
     */
    public function getSystem()
    {
        return 'crocus';
    }

    /**
     * @param mixed $data
     * @return bool
     * */
    public static function primary($data): bool
    {
        return $data instanceof Orders;
    }
}