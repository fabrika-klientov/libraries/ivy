<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Adapters\Helpers;


use Illuminate\Support\Str;

trait Normalize
{
    /**
     * @override
     * @param bool $normalize
     * @return string
     * */
    public function getPhone(bool $normalize = true)
    {
        return $normalize ? $this->normalize(parent::getPhone()) : parent::getPhone();
    }

    /** get normalizing phone number
     * @param string $phone
     * @return string
     * */
    protected function normalize($phone)
    {
        if (isset($this->configs['phoneNumberFormat']) && !empty($this->configs['phoneNumberFormat'])) {
            $format = $this->configs['phoneNumberFormat'];

            return $this->getNormalizeOfFormat($phone, $format);
        }

        return $phone;
    }

    /** full phone number
     * @param string $phone
     * @param string $format
     * @return string
     * */
    protected function getNormalizeOfFormat($phone, $format)
    {
        $phone = trim(str_replace('/[^+\d]/', '', $phone));

        if (Str::length($phone) > 10) {
            return '+' . str_replace('+', '', $phone);
        }

        switch ($format) {

            case '380':
                return '+' . $format . substr($phone, -9);

            case '7':
                return '+' . $format . substr($phone, -10);

                // push other
        }

        return $phone;
    }
}