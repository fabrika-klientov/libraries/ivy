<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Helpers
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Helpers;


use Ivy\Contracts\BeAdapter;

trait ActionsAdapter
{
    /**
     * @var array $supportedTypesAdapter of adapters
     * */
    protected $supportedTypesAdapter = ['orders'];

    /**
     * @var array $supportedAdapters of adapters
     * */
    protected $supportedAdapters = [
        'orders' => [],
    ];

    /**
     * @param array $collect ['\Ivy\Adapters\Crocus\OrdersAdapter', OrdersAdapter::class]
     * @param string $type (orders)
     * @return void
     * @throws \Exception
     * */
    public function initAdapters(array $collect, string $type)
    {
        $this->validateTypeAdapter($type);

        foreach ($collect as $item) {
            $this->validateAdapter($item);
        }
        $this->supportedAdapters[$type] = $collect;
    }

    /**
     * @param string $class
     * @param string $type
     * @return $this
     * @throws \Exception
     * */
    public function pushAdapter(string $class, string $type)
    {
        $this->validateTypeAdapter($type);
        $this->validateAdapter($class);
        $this->supportedAdapters[$type][] = $class;

        return $this;
    }

    /**
     * @param string $class
     * @return $this
     * @throws \Exception
     * */
    public function removeAdapter(string $class)
    {
        foreach ($this->supportedAdapters as $type => &$supportedAdapters) {
            $index = (int)array_search($class, $supportedAdapters);
            if ($index !== false) {
                array_splice($supportedAdapters, $index, 1);
            }
        }

        return $this;
    }

    /**
     * @param string $class
     * @return void
     * @throws \Exception
     * */
    protected function validateAdapter(string $class)
    {
        if (class_exists($class) && is_subclass_of($class, BeAdapter::class)) {
            return;
        }

        throw new \Exception('Class [' . $class . '] not valid. Should implements [BeAdapter]');
    }

    /**
     * @param string $type
     * @return void
     * @throws \Exception
     * */
    protected function validateTypeAdapter(string $type)
    {
        if (in_array($type, $this->supportedTypesAdapter)) {
            return;
        }

        throw new \Exception('Type [' . $type . '] not valid. Should in ' . json_encode($this->supportedTypesAdapter));
    }
}