<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Helpers
 * @category  Ivy
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.25
 * @link      https://fabrika-klientov.ua
 */

namespace Ivy\Helpers;

use Illuminate\Support\Collection;

/**
 * @method Collection orders(Collection $data, $client = null)
 * */
trait InstancesAdapter
{

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        switch ($name) {

            case 'orders':
                /**
                 * @var \Illuminate\Support\Collection|null $collect
                 * */
                $collect = head($arguments);
                $client = count($arguments) == 2 ? last($arguments) : null;

                if (is_null($collect)) {
                    return collect();
                }

                return $collect->reduce(function (Collection $result, $item) use ($name, $client) {
                    $class = $this->getClassOfAdapter($item, $name);

                    if (isset($class)) {
                        $result->push(new $class($item, $client));
                    }

                    return $result;
                }, new Collection());

        }

        return null;
    }

    /** get adapter
     * @param mixed $data
     * @param string $type
     * @return string|null
     * */
    public function getClassOfAdapter($data, string $type)
    {
        try {
            $this->validateTypeAdapter($type);
        } catch (\Exception $e) {
            return null;
        }

        foreach ($this->supportedAdapters[$type] as $adapter) {
            if ($adapter::primary($data)) {
                return $adapter;
            }
        }

        return null;
    }
}