<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 29.02.20
 * Time: 13:40
 */

namespace Tests;

use Ivy\IvyService;
use PHPUnit\Framework\TestCase;

class IvyServiceTest extends TestCase
{

    public function test__construct()
    {
        $service = new IvyService();

        $this->assertInstanceOf(IvyService::class, $service);
    }

    public function testContext()
    {
        new IvyService();

        $this->assertInstanceOf(IvyService::class, IvyService::context());
    }
}
