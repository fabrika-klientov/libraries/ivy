## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/ivy/v/stable)](https://packagist.org/packages/shadoll/ivy)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/ivy/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/ivy/commits/master)
[![coverage report](https://gitlab.com/fabrika-klientov/libraries/ivy/badges/master/coverage.svg)](https://gitlab.com/fabrika-klientov/libraries/ivy/commits/master)
[![License](https://poser.pugx.org/shadoll/ivy/license)](https://packagist.org/packages/shadoll/ivy/)

**Library for MarketPlaces**

---

Сервис соединяющий маркетплейсы

_2020-02-29_
- [crocus](https://gitlab.com/fabrika-klientov/libraries/crocus)
- [riccia](https://gitlab.com/fabrika-klientov/libraries/riccia)

---


## Install

`composer require shadoll/ivy`


## us

В продуктах `laravel/lumen` можно подключить сервис провайдер `Ivy\Lara\Providers\ServiceProvider` в
конфиг `configs/app.php` laravel или `bootstrap/app.php` lumen.
Создать Свой `Middleware` наследуясь от `Ivy\Lara\Middleware\BaseAuthBurdock` и подключить на ваш роутинг
или jobs где вам необходимо.

Перед использованием Сервиса Ivy необходимо зареестрировать адаптеры с которыми будете работать
Это можно сделать в вашем `Middleware`

```php

$service = new IvyService();
// регистрация адаптеров
$service->initAdapters([
    \Ivy\Adapters\Crocus\OrdersAdapter::class,
    \Ivy\Adapters\Riccia\OrdersAdapter::class,
], 'orders');
// добавить адаптер
$service->pushAdapter('any\Adapter\Class', 'orders');
// удалить адаптер
$service->removeAdapter(\Ivy\Adapters\Riccia\OrdersAdapter::class);

// Получить необходимый адаптер для данных с маркетплейсов 
// Возвращает Коллекцию адаптеров Обьект интерфейса
// Для orders
$clientRiccia = new \Riccia\Client([
   "username" => "login",
   "password" => "pass...",
]);
/**
 * @var \Illuminate\Support\Collection<\Ivy\Contracts\BeOrdersAdapter> $adapters
 **/
$adapters = $service->orders($clientRiccia->orders->get()); // вторым параметром можно передать клиент (опционально)
$adapters = $service->orders($clientRiccia->orders->get(), $clientRiccia);


```